# 1장 DOM 스크립팅의 기초 
- - -
1. HTML5- HTML5의 의의[바로가기](https://gitlab.com/choan/HTML5API#1-html5의-의의)
2. DOM 트리[바로가기](https://gitlab.com/choan/HTML5API#2-dom-트리)
3. DOM 트리 접근자[바로가기](https://gitlab.com/choan/HTML5API#3-dom-트리-접근자)
4. HTML 문서 리소스의 정보[바로가기](https://gitlab.com/choan/HTML5API#4-HTML-문서-리소스의-정보)
5. 마크업 삽입[바로가기](https://gitlab.com/choan/HTML5API#5-마크업-삽입)
6. 요소와 속성 제어[바로가기](https://gitlab.com/choan/HTML5API#6-요소와-속성-제어)
7. Selectors API[바로가기](https://gitlab.com/choan/HTML5API#7-Selectors-API)
8. 이벤트[바로가기](https://gitlab.com/choan/HTML5API#8-이벤트)
9. 타이머[바로가기](https://gitlab.com/choan/HTML5API#9-타이머)
10. 모달 창[바로가기](https://gitlab.com/choan/HTML5API#10-모달-창)
11. Location API[바로가기](https://gitlab.com/choan/HTML5API#11-Location-API)
12. History API[바로가기](https://gitlab.com/choan/HTML5API#12-History-API)
13. Navigator 객체[바로가기](https://gitlab.com/choan/HTML5API#13-Navigator-객체)
14. 브라우저 인터페이스[바로가기](https://gitlab.com/choan/HTML5API#14-브라우저-인터페이스)
15. 포커스의 의의[바로가기](https://gitlab.com/choan/HTML5API#15-포커스)


* * *
## 1. HTML5의 의의
이전까지 자바스크립트에서 당연하게 사용되던 기능을 HTML5에서 표준 API 화 하여 브라우저간 차이가 없도록 명세

* * *

## 2. DOM 트리
- DOM(Document Object Model) -  HTML 문서 내의 컨텐츠를 객체 개념으로 전환하여 HTML 문서의 컨텐츠에 접근할 수 있게 API를 정의한 것
- DOM 트리 - 태그의 계층 구조를 그대로 트리로 만든 것
- HTML5 명세 이전에는 브라우저별로 DOM 트리 렌더링 방식이 상이 했음. (대표적인 것이 화이트스페이스 - IE8에서는 화이트스페이스를 포함하지 않았으나 HTML5에서 화이트스페이스를 텍스트 노드에 포함)

* * *

## 3. DOM 트리 접근자
- DOM 트리 접근자 - HTML 문서의 컨텐츠를 제어하기 위해 해당 요소를 자바스크립트에서 참조하는 수단
    - document.head : head 요소의 객체를 반환. head 요소가 없다면 null 반환 / 읽기전용
    - document.title : title 요소에 있는 문서의 제목을 반환 title 요소가 없다면 공백 문자열 반환 /
    - document.body : body 요소의 객체 반환
    - document.images : HTML 내의 모든 img 요소가 담긴 목록을 나타내는 HTMLCollection 객체를 반환 / 읽기전용
    - document.embeds / document.plugins : HTML 내의 embed 요소가 담긴 목록을 나타내는 HTMLCollection 객체를 반환 / 읽기전용
    - document.links : HTML 내의 href 속성이 있는 모든 a 요소와 area 요소의 목록을 나타내는 HTMLCollection 객체를 반환 / 읽기전용
    - document.forms : HTML 내의모든 form 요소가 담긴 목록을 나타내는 HTMLCollection 객체를 반환 / 읽기전용
    - document.scripts : HTML 내의모든 script 요소가 담긴 목록을 나타내는 HTMLCollection 객체를 반환 / 읽기전용
- HTMLCollection 객체의 속성과 함수
    - HTMLCollection 객체는 특정요소가 담긴 목록 / Array 객체와 다름
    - collection.length : 목록 수 반환 / 읽기전용
    - collection.item(index) / collection[item] / collection(index) : 매개변수(index)에 지정된 위치에 담긴 요소의 노드 객체를 반환/ index는 0부터 시작/ 목록 갯수 이상의 숫자를 index에 지정하면 null 반환
    - collection.namedItem(name) / collection[name] / collection(name) : 매개변수와 name 속성이나 id의 속성값이 일치하는 요소를 반환. 해당 요소 여러개 있으면 첫번째 요소 반환. 지정한 요소가 없다면 null 반환
    -  HTMLCollection 객체는 실시간 - Array 객체와 달리 HTMLCollection 객체는 동적으로 내용이 변한다.

- DOM 트리에 접근하는 함수
    - document.getElementsByName(name) : name 속성값이 매개변수 name과 같은 요소의 목록을 NodeList 객체로 반환 (namedItem() 함수 없음)
    - document.getElementsByClassName(클래스명1 클래스명2 ) / element.getElementsByClassName(클래스명) : 매개변수로 지정한 값이 클래스 속성값과 일치하는 요소의 목록을 NodeList 객체로 반환 / 빈칸으로 구분하여 클래스 이름을 여러개 설정할 수 있음 / document 객체나 요소 노드 객체(element)에서 사용가능
    - document.getElementsById(아이디명) : 매개변수(아이디명)와 id 속성값이 일치하는 요소의 객체를 반환 / 없으면 null 반환
    - document.getElementsByTagName(태그명) / element.getElementsByTagName(태그명) : 매개변수(태그명)와 일치하는 요소의 목록을 NodeList 객체로 반환

* * *
 
## 4. HTML 문서 리소스의 정보
- URL 정보
    - document.URL : HTML 문서의 주소를 반환 / 읽기전용 - 페이지를 이동시킬수 없음
    - document.referrer : 해당 문서의 유입 경로 주소 반환 / 유입 경로 페이지 주소 정보 없으면 빈 문자열 반환/ 읽기전용
        - 직접 url을 입력하거나 rel 속성값을 noreferrer로 설정한 a 태그의 링크로 유입된 경우 값이 없다.
        - ssl로 보호된 페이지에서 보호되지 않는 페이지로 이동해도 유입 경로 얻을 수 없다.

- 최종 갱신 일시
    - document.lastModified : 웹서버가 전송해준 HTTP response header의 Last-Modified로부터 얻어진 문서 최종 갱신 일시 반환
    - MM/DD/YYYY hh : mm : ss 와 같은 형식으로 표기/읽기 전용
    - 웹서버가 HTTP response header의 Last-Modified를 반환하지 않으면 브라우저에 따라 현재 일시를 반환

- 렌더링 모드
    - document.compatModeA : HTML  문서가 어떤 모드로 브라우저에서 렌더링되는 지를 나타내는 문자열 반환. HTML5 표준 문서는 CSS1Compat / 이전 버전 문서는 BackCompat 반환 / 읽기전용

- 문자 인코딩
    - document.charset : HTML 문서의 문자 인코딩을 반환 / 동적으로 값 설정 가능
    - document.characterSet : HTML 문서의 문자 인코딩을 반환 / 읽기전용
    - document.defaultCharset : 브라우저에 설정되어 있는 인코딩의 기본값을 반환/ 읽기전용

- 문서 로딩 상태
    - document.readyState : HTML 문서를 읽어들이는 중이면 "loading" / 다 읽었다면 "complete" 반환
    - 속성값이 변하면 readystatechange 이벤트가 document 객체에서 발생 / 읽기전용
    - window 객체에서 load 이벤트가 발생하기 직전에 document.readyState 속성값은 "complete"
    - 크롬, 파폭, ie9 이상에서는 "loading"과 "complete" 사이에 "interactive"가 발생

* * *

## 5. 마크업 삽입
- document.write(text) : 매개변수로 지정한 문자열을 그대로 HTML에 삽입
- document.writeIn(text) : 매개변수로 지정한 문자열 마지막에 줄바꿈 문자를 추가하여 HTML에 삽입
- document.innerHTML : HTML 문서전체의 HTML을 문자열로 반환.
- element.innerHTML : 요소의 컨텐츠를 나타내는 HTML을 문자열로 반환
- element.outerHTML : 요소와 컨텐츠를 나타내는 HTML 문자열을 반환
- element.insertAdjacentHTML(beforebegin | afterbegin | beforeend | afterend , 삽입할 텍스트) : 요소의 컨텐츠를 나타내는 HTML에 삽입 위치를 나타내는 키워드를 지정하여 컨텐츠를 다시 쓴다.

    - 첫번째 매개변수는 텍스트를 삽입할 위치, 두번째 매개변수는 삽입할 텍스트
    - beforebegin - 요소 바로 앞
    - afterbegin - 요소 시작태그 바로 뒤 (요소의 first-child로 삽입)
    - beforeend - 요소의 종료태그 바로 앞 (요소의 last-child 로 삽입)
    - afterend - 요소끝의 바로 뒤
- document.implementation.createHTMLDocument(title) : HTML 문서를 새로 생성하여 document 객체를 반환. 새로 생성된 HTML 문서는 매개변수에 지정된 문자열로 타이틀 요소를 설정
    - XMLHttpRequest를 이용하여 동적으로 HTML 식별자를 서버로부터 취득해서 DOM을 제어할 때 유용
    - 이때 XMLHttpRequest로 얻을 수 있는 건 document 객체가 아닌 HTML 마크업 텍스트
    - DOM으로 HTML을 제어하고 싶으면 creteHTMLDocument() 함수로 필요한 최소의 HTML 문서의 document 객체를 생성하여, innerHTML 속성을 써서 삽입할 수 있다.

- document.createTextNode(데이터) : 텍스트 노드를 생성 - 보안상의 이유로 입력받은 값으로 인해 보안상의 문제나 HTML 문서에 영향을 주지 않도록 하기 위해 텍스트 처리를 해야 할 경우 사용할 수 있다.
- element.textContent : 요소를 텍스트 처리해주는 속성

* * *

## 6. 요소와 속성 제어
- 요소를 제어하는 API
    - element.tagName : 요소명을 반환 / 대소문자 상관없이 대문자로 변환하여 반환/ 읽기전용
    - document.createElement(태그명) : 매개변수로 지정한 요소명의 element 객체를 생성하여 반환/ 브라우저 내부에서 소문자로 변환됨
    - element.setAttribute(속성명, 값) : 요소에서 매개변수로 지정한 값을 가진 속성을 설정/ 소문자로 변환
    - element.getAttribute(속성명) : 요소로부터 매개변수에 지정된 속성값을 반환 / 소문자로 변환

- class를 제어하는 API

    - 토큰 : class를 구분하는 키워드
    - 토큰은 대소문자 구별 / 공백문자 에러 발생
    - DOMTokenList 객체 : 토큰 목록 - classList 속성에서 얻을 수 있다.
    - element.classList : 요소에 설정된 class 속성의 토큰 목록을 DOMTokenList 객체로 반환
    - DOMTokenList.length : 토큰 수 반환 / 읽기전용
    - DOMTokenList.item(index) : 매개변수(index)에 지정된 위치에 담긴 토큰 반환. index a.k.a. 0부터 시작 / 담긴 갯수 이상으로 index를 조회하면 null 반환
    - DOMTokenList.contains(token) : 매개변수(token)이 목록에 있으면 true, 없으면 fasle 반환
    - DOMTokenList.add(token): 매개변수에 지정한 토큰을 목록에 추가.
    - DOMTokenList.remove(token): 매개변수에 지정한 토큰을 목록에서 제거
    - DOMTokenList.toggle(token): 매개변수에 지정한 토큰이 목록에 있으면 제거, 없으면 추가 (toggle) / 토큰을 추가하면 true, 제거하면 fasle 반환
    - DOMTokenList 객체는 link,a,area 요소의 rel 속성(element.relList)도 규정

- 커스텀 데이터 속성
    - data-커스텀 속성 : HTML5에서 새로 생긴 커스텀 속성
    - element.dataset : 요소에 설정된 커스텀 데이터 속성을 가진 DOMStringMap 객체를 반환
    - 일반적인 호출 형태는 dataset.속성명
    - 커스텀 속성명에 하이픈이 있으면 dataset["속성-명"] 으로 표기

- [예제](http:/choan616.dothome.co.kr/study/html5/01_DOM_Scripting/toggle_token.html/) / [소스 예제](https://gitlab.com/choan/HTML5API/blob/master/1.DOM_%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8C%85%EC%9D%98_%EA%B8%B0%EC%B4%88/toggle_token.html)

* * *

## 7. Selectors API
- CSS 선택자로 요소에 접근하는 API
- document.querySelector(선택자) / element.querySelector(선택자) : 문서나 요소 안에서 매개변수로 지정한 선택자와 일치하는 요소 중 가장 첫 번째로 발견된 요소의 노드 객체 반환 / 일치하는 요소가 여러개 있어도 반환되는 것은 최초의 1개뿐. 일치 요소 없으면 null 반환
- document.querySelectAll(선택자) / element.querySelectAll(선택자) : 문서나 요소 안에 매개변수로 지정한 선택자와 일치하는 요소의 노드 객체의 목록을 담고 있는 NodeList 객체를 반환. 일치요소 없으면 요소의 수가 0인 NodeList 객체 반환
- 선택자 패턴
    - "*" : 모든 요소
    - E : E 요소
    - E[foo] : E 요소중 "foo" 속성을 가진 요소 / 값의 유무 여부는 상관 없음
    - E[foo="bar"] : E 요소 중 "foo" 속성값이 "bar" 인 것
    - E[foo~="bar"] : E 요소 중 "foo" 속성값이 공백 문자로 구별된 키워드의 목록이고 그 중에 키워드 하나가 "bar"와 일치하는 것
    - E[foo^="bar"] : E 요소 중 "foo" 속성값이 "bar"로 시작하는 것
    - E[foo$="bar"] : E 요소 중 "foo" 속성값이 "bar"로 끝나는 것
    - E[foo*="bar"] : E 요소 중 "foo" 속성값에 "bar"가 들어가는 것
    - E[foo|="en"] : E 요소 중 "foo" 속성값이 하이픈으로 구별된 키워드 목록이며 맨 좌측 값이 "en"에 일치하는 것
    - E:root : E 요소 중 문서의 루트가 되는 것 (html 문서는 html 요소를 반환)
    - E:nth-child(n) : E 요소 중 부모 요소를 기준으로 n번째 자식 요소
    - E:nth-last-child(n) : E 요소 중 부모 요소를 기준으로 마지막에서 n번째 자식 요소
    - E:nth-of-type(n) : E 요소 중 부모 요소를 기준으로 n번째의 E 요소가 되는 자식 요소에 해당하는 것
    - E:nth-last-of-type(n) : E 요소 중 부모 요소를 기준으로 n번째의 E 요소가 되는 자식 요소에 해당하는 마지막 E 요소
    - E:first-child : E 요소 중 부모 요소를 기준으로 첫 번째 자식 요소. E:nth-first-child(1)과 같다.
    - E:last-child : E 요소 중 부모 요소를 기준으로 마지막 자식 요소. E:nth-last-child(1)과 같다.
    - E:first-of-type: E 요소 중 부모 요소를 기준으로 최초의 E 요소. E:nth-of-type(1)과 같다
    - E:last-of-type : E 요소 중 부모 요소를 기준으로 마지막 E 요소. E:nth-last-of-type(1)과 같다
    - E:only-child : E 요소 중 부모 요소를 기준으로 유일한 자식 요소
    - E:empty : E 요소 중 텍스트 노드를 포함하여 자식 요소가 하나도 없는 것
    - E:link, E:visited : E 요소 중 :link, :visited 인 것
    - E:active / E:hover / E:focus
    - E:target : E 요소 중 href의 target이 되는 것
    - E:lang(fr) : E 요소에서 언어가 "fr"인 것
    - E:enabled / E:disabled : E 요소의 UI 요소 중에 enabled/disabled 한 것
    - E:checked : E 요소의 UI 요소 중에 checked 된 것
    - E.warning : E 요소 중 class가 "waring"인 것
    - E#myid : E 요소 중 id가 "myid"인 것
    - E:not(s) : E 요소 중 지정한 선택자 s와 일치하지 않는 것
    - E F : E 요소의 자손에 해당하는 F요소
    - E > F : E 요소의 자식 요소에 해당하는 F 요소
    - E + F : E 요소 바로 뒤에 오는 F요소
    - E ~ F : E 요소 보다 뒤에 오는 F요소
    - :visited 유사 클래스는 개인정보등에 접근할 수 있기 때문에 보안 문제가 발생할 수 있으므로 연결된 페이지에 방문 여부를 판단하는 처리는 자제하는 것이 좋다.

- Selectors API가 반환하는 NodeList는 실시간이 아님

* * *

## 8. 이벤트
- 이벤트 핸들러 - 오직 하나만 정의
    - window.onload = function () {...}

- 이벤트 리스너 - 여러개 정의 가능
    - window.addEventListener("load", function() {...}, false);
    - load 이벤트
    - 자바스크립트 파일, css, css 호출 이미지, 페이지 호출 이미지를 로딩한 다음에 load 이벤트 발생

- DOMContentLoaded 이벤트
    - HTML 파일 로딩 시 DOM 트리가 구성되면 스크립트 실행 - 문서만 로드되면 실행
    - document 객체에서 발생

* * *

## 9. 타이머
- 일정한 시간이 지나면 특정한 처리를 실행하는 기능
- handle = window.setTimeOut(callback, 지정시간) || handle = window.setTimeout(callback, 지정시간, 매개변수,...) :

    - 타이머를 설정. 지정시간은 밀리 초 단위로 표기.
    - 지정 시간 후에 콜백 함수 실행.
    - 지정 시간을 생략하면 0으로 간주
    - 매개변수는 콜백함수에 전달.
    - 이 함수는 타이머를 식별하는 ID(handle)을 반환

- window.clearTimeout(handle) : setTimeout() 함수가 반환한 handle을 매개변수로 전달하면 해당 타이머를 취소
- handle = window.setInterval(callback, 지정시간) || handle = window.setInterval(callback, 지정시간, 매개변수,...) :
    - 반복 타이머 설정
    - 지정 시간 간격으로 callback 함수 실행
    - 매개 변수는 callback 함수에 전달
    - 이 함수는 타이머를 식별하는 ID(handle)을 반환

- window.clearInterval(handle) : setInterval() 함수가 반환한 handle을 매개변수로 전달하면 해당 반복 타이머를 취소
- [예제](http://choan616.dothome.co.kr/study/html5/01_DOM_Scripting/timer.html)/ [소스 예제](https://gitlab.com/choan/HTML5API/blob/master/1.DOM_%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8C%85%EC%9D%98_%EA%B8%B0%EC%B4%88/timer.html)

* * *

## 10. 모달 창
- window.alert(메시지): 매개변수의 텍스트를 내용으로 표시하는 경고창
- window.confirm(메시지) : 매개변수의 텍스트를 내용으로 표시하는 확인창 (확인(true반환), 취소(false반환) 버튼이 있음)
- window.prompt(메시지) / window.prompt(메시지, 기본값):

    - 매개변수의 텍스트를 내용으로 표시하는 입력창.
    - 일반적으로 입력창에 텍스트필드, 확인, 취소 버튼으로 구성
    - 확인 클릭 - 텍스트필드에 입력된 문자를 반환, 취소 클릭 - null 반환
    - 기본값 매개변수에 텍스트를 지정하면, 입력필드에 기본값을 설정해서 입력창이 표시

- window.showModalDialog(url, argument) :

    - 첫번째 매개변수(url)에 지정한 페이지를 대화상자로 표시
    - 두번째 매개변수(argument)에는 대화상자에 넘기고 싶은 값을 지정할 수 있다.
    - 모달창을 종료했을 때 대화상자에서 지정한 값을 반환
    - 명세에는 세번째 매개변수를 규정하지 않았으나, 대부분의 브라우저에서 세번째 매개변수를 허용하여 커스텀 모달창의 사이즈를 규정할 수 있음
    - window.dialogArguments :  대화상자를 호출한 페이지의 showModalDialog()함수의 두번째 매개변수(argument)에 지정된 값이 담겨있음 / 읽기전용
    - window.returnValue :

        - 값을 지정하면 그 값이 대화상자를 호출한 페이지의 showModalDialog() 함수의 반환값이 된다.
        - 값을 지정하지 않으면 브라우저별로 반환하는 값이 다르므로 null이나 공백문자를 지정하여 브라우저 간에 차이가 발생하지 않도록 한다.

    - 팝업 차단 대상이므로 팝업 차단 경고가 표시됨

* * *

## 11. Location API
- window.location / document.location :

    - 현재 페이지의 URL을 나타내는 Location 객체를 반환.
    - URL을 설정하면 Location 객체는 href 속성을 변경해서 해당 페이지로 이동 가능
    - 방문기록을 남기고 페이지 이동함
    - location.href : 현재 페이지의 URL을 반환 / 설정하여 페이지를 이동할 수 있다./ 방문기록을 남기고 페이지 이동함
    - location.assign(url): 매개변수 페이지로 이동 / 방문기록을 남기고 페이지 이동함
    - location.replace(url): 현재 페이지를 매개변수(url)페이지로 이동 / 방문기록을 덮어 씀
    - location.reload(): 현재 페이지를 다시 읽어 들임
    - location.resolveURL(url):매개변수(url)에 상대 경로를 지정하면 절대 경로로 변환한 값을 반환
    - location.protocol : 스키마 (프로토콜) - http:
    - location.host : 호스트와 포트번호(도메인 주소:포트번호)
    - location.hostname : 호스트
    - location.port: 포트번호
    - location.pathname : 페이지 경로명(/폴더/파일.html)
    - location.search: 쿼리
    - location.hash: 플래그 식별자
    - [예제](http://choan616.dothome.co.kr/study/html5/01_DOM_Scripting/location.html)/[소스예제](https://gitlab.com/choan/HTML5API/blob/master/1.DOM_%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8C%85%EC%9D%98_%EA%B8%B0%EC%B4%88/location.html)

* * *

## 12. History API
- 방문기록은 브라우저 탭 별로 저장되는 데 탭을 닫으면 삭제된다. 이러한 기록을 세션 기록이라 한다.

- 세션 기록에 접근하는 함수와 속성
    - window.history.length : 세션 기록에 기록된 항목의 수를 반환 / 읽기전용
    - window.history.go([delta]):

        - 매개변수(delta)에 값을 지정하면 현재 페이지에서 지정한 수만큼 기존 페이지로 이동
        - 음수를 지정하면 뒤로, 양수를 지정하면 앞으로 이동
        - 매개변수는 옵션
        - 0을 지정하면 현재 페이지 다시 호출 - window.location.reload();

    - window.history.back(): 세션 기록에 기록된 이전 항목 페이지로 이동 - window.history.go(-1)과 같다.
    - window.history.forward(): 세션 기록에 기록된 다음 항목 페이지로 이동 - window.history.go(1)과 같다.
    - window.history.pushState(data, title[, url]):

        - 현재 페이지의 상태에 대한 항목을 새로 추가
        - 세션 기록에 현재 항목 이휴에 항목이 있으면 모두 삭제
        - 첫번째 매개 변수에는 popstate 이벤트 발생 시 이벤트 객체에 전달하는 정보를 설정
        - 두번째 매개 변수는 해당 방문 기록 세션의 제목을 지정 - 방문 기록을 뒤로가기할 때 참조하는 정보.
        - 세번째 매개변수는 선택적으로 지정하거나 지정하지 않을 수 있다. 해당 화면에 고유한 url을 지정하고 싶으면 지정.
    	- [예제](http://choan616.dothome.co.kr/study/html5/01_DOM_Scripting/pushState.html)/ [소스 예제](https://gitlab.com/choan/HTML5API/blob/master/1.DOM_%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8C%85%EC%9D%98_%EA%B8%B0%EC%B4%88/pushState.html)

    - window.history.replaceState(data, title [, url]): 현재 페이지에 해당하는 항목을 업데이트함.

        - 첫번째 매개 변수에는 popstate 이벤트 발생 시 이벤트 객체에 전달하는 정보를 설정
        - 두번째 매개 변수는 해당 방문 기록 세션의 제목을 지정 - 방문 기록을 뒤로가기할 때 참조하는 정보.
        - 세번째 매개변수는 선택적으로 지정하거나 지정하지 않을 수 있다. 해당 화면에 고유한 url을 지정하고 싶으면 지정.

- 세션 기록 관련 이벤트
    - window 객체에서 발생한다.
    - popstate 이벤트 :
        - 세션 기록의 항목이 브라우저에 페이지를 로드할 때마다 발생. state 속성을 설정
        - event.state : window.history.pushState() 함수와 window.history.replaceState() 함수의 첫번째 매개변수로 설정된 값이 담긴다.없으면 null을 설정
        - [예제](http://choan616.dothome.co.kr/study/html5/01_DOM_Scripting/popState.html)/ [소스 예제](https://gitlab.com/choan/HTML5API/blob/master/1.DOM_%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8C%85%EC%9D%98_%EA%B8%B0%EC%B4%88/popState.html)

    - hashchange 이벤트
        - 세션 기록의 항목으로 이동할때 전후 항목의 url이 플래그 식별자 값(# 이후의 값) 만 다르면 hashchange 이벤트가 발생
        - event.oldURL : 이동하기 전 항목의 URL을 반환 / 읽기 전용
        - event.newURL : 이동 후(현재의 URL을 반환) / 읽기 전용

    - pageshow 이벤트
        - 세션 기록을 이동해서 다른 페이지에 도착하면 도착한 페이지가 브라우저에 읽어들여질 때 window 객체에서 pageshow 이벤트 발생
        - url의 쿼리부분(? 이후 부분)이 다르면 이 이벤트가 발생
        - persisted 속성 지정
        - event.persisted : 이전에 표시된 화면의 상태를 유지하여 표시할 것을 지정. (이전에 표시된 화면의 상태 : 스크롤 위치, 텍스트필드에 입력된 문자 등) / 처음 접속하면 false, 이전에 표시한 적이 있는 페이지면 true / 읽기전용

    - pagehide 이벤트
        - 세션 기록을 이동했을 때 도착점이 다른 페이지면 페이지 이동하기 전 페이지의 window 객체에서 pagehide 이벤트 발생
        - persisted 속성 지정
        - 보통 true 지정 / A.K.A 읽기전용

* * *

## 13. Navigator 객체
- 브라우저의 여러가지 상태를 나타내는 정보가 있다.
- Navigator 객체의 브라우저 식별 정보 속성
    - window.navigator.appName : 브라우저의 이름 반환 / 읽기 전용
    - window.navigator.appVersion : 브라우저의 버전을 반환 / 읽기 전용
    - window.navigator.platform : OS 이름을 반환 / 읽기전용
    - window.navigator.userAgent : 웹 서버에 송신하는 User-Agent 헤더의 값을 반환 / 읽기전용

- Navigator 객체의 네트워크 접속정보 속성
    - window.navigator.onLine : 확실하게 네트워크에 접속되지 않았다고 판단하면 false, 반대의 경우 true / 읽기전용
- [예제](http://choan616.dothome.co.kr/study/html5/01_DOM_Scripting/navigator.html) / [소스 예제](https://gitlab.com/choan/HTML5API/blob/master/1.DOM_%EC%8A%A4%ED%81%AC%EB%A6%BD%ED%8C%85%EC%9D%98_%EA%B8%B0%EC%B4%88/navigator.html)

* * *

## 14. 브라우저 인터페이스
- 각종 바(bar) 표시에 대한 속성
    - window.locationbar.visible : 주소창 표시 true . 표시하지 않으면 false
    - window.menubar.visible : 메뉴바 표시 true . 표시하지 않으면 false
    - window.personalbar.visible :내 즐겨찾기 표시 true . 표시하지 않으면 false
    - window.scrollbars.visible : 스크롤바 표시 true . 표시하지 않으면 false
    - window.statusbar.visible : 상태바 표시 true . 표시하지 않으면 false
    - window.toolbar.visible : 툴바 표시 true . 표시하지 않으면 false

* * *

## 15. 포커스
- 문서 포커스에 관한 속성 및 함수
    - document.activeElement : 문서 안에 포커싱된 요소가 있다면 요소의 객체를 반환. 없으면 body 요소의 객체 반환
    - document.hasFocus() : 문서가 포커싱 되어 있다면 true, 없으면 false 반환
    - window.focus() [비추천] : 윈도를 포커싱. HTML5 명세에는 이 함수를 호출하면 사용자에게 어떠한 알림창을 브라우저에 띄울 것을 추천
    - window.blur() [비추천] : 윈도의 포커스를 없앰. HTML5 명세에는 이 함수를 호출해도 브라우저에서 무시할 것을 추천

- 요소의 포커스
    - element.focus() : 요소에 포커스를 준다
    - element.blur() [비추천] : 요소에서 포커스를 제거

* * *

# 4장 비디오와 오디오
- - -
1. 마크업의 개요
2. video 요소
3. audio 요소
4. 코덱
5. source 요소
6. MIME 형식으로부터 재생 가능 여부를 판단
7. 선택된 파일 확인
8. 네트워크 이용상태 파악
9. 재생과 정지
10. 미디어 리소스 로드
11. 미디어 데이터의 로드 상태 파악
12. 재생 속도
13. 길이와 재생 위치
14. 재생 완료와 버퍼링 완료의 범위
15. 탐색
16. 음량 조절
17. 오류 처리
18. 이벤트
19. 커스텀 플레이어
20. 비디오와 canvas의 조합
21. 자막 넣기

* * *
## 1. 마크업의 개요
- video, audio 요소 : 플래시나 실버라이트 같은 플러그인을 사용하지 않고 미디어 파일의 재생이 가능
* * *

## 2. video 요소
- src 속성 : 비디오 파일의 url 지정
- width, height : 비디오의 가로 세로
- 대체 컨텐츠 : video 요소를 지원하지 않는 브라우저를 위한 컨텐츠
- poster : 비디오 재생을 하기전 표시할 기본 이미지
- preload : 미디어 데이터를 미리 받아둘지를 결정
	- none : 비디오가 재생 가능해지기 전까지 내려받지 않음
	- metadata : 비디오파일의 메타 정보가 담긴 부분까지만 미리 내려받음
	- auto : 미리 많은 부분을 내려받음
	- 브라우저별로 지원하는 형태가 다름. 대부분의 브라우저는 none으로 지정해도 내려받는다.
	- 네트워크 상태나 디바이스 환경에 따라 브라우저가 최종적으로 판단하며, 브라우저에 전달하는 참고값
	- autoplay 속성을 지정하면 preload 속성은 무시됨
- autoplay : 비디오가 재생 가능해지면 즉시 재생하도록 지정하는 불리언 속성
- loop : 반복 재생 여부를 지정하는 불리언 속성
- controls : 비디오의 컨트롤 UI 표시를 지정하는 불리언 속성
- videoWidth : 실제 영상의 가로길이 반환 / 읽기 전용
- videoHeight : 실제 영상의 세로길이 반환 / 읽기 전용 
- video 요소 지원 여부 판별 : window.HTMLVideoElement 객체 여부 확인
* * *

## 3. audio 요소
- controls="controls" || controls  속성을 지정하면 브라우저에 구현된 audio 관련 UI를 표시
- UI는 브라우저별로 다름
- src : 파일의 URL 지정
- preload : 미디어 데이터를 미리 받아둘지를 결정
	- none : 비디오가 재생 가능해지기 전까지 내려받지 않음
	- metadata : 비디오파일의 메타 정보가 담긴 부분까지만 미리 내려받음
	- auto : 미리 많은 부분을 내려받음
	- 브라우저별로 지원하는 형태가 다름. 대부분의 브라우저는 none으로 지정해도 내려받는다.
	- 네트워크 상태나 디바이스 환경에 따라 브라우저가 최종적으로 판단하며, 브라우저에 전달하는 참고값
	- autoplay 속성을 지정하면 preload 속성은 무시됨
- autoplay : 비디오가 재생 가능해지면 즉시 재생하도록 지정하는 불리언 속성
- loop : 반복 재생 여부를 지정하는 불리언 속성
- controls : 비디오의 컨트롤 UI 표시를 지정하는 불리언 속성
- Audio()를 생성자로 호출하면(new 키워드 이용)  audio요소를 자바스크립트로 간단히 생성할 수 있다.
- audio 요소 지원 여부 판별 : window.HTMLAudioElement 객체 여부 확인
* * *

## 4. 코덱
- 코덱 : 비디오나 오디오의 인코딩 방식
- 비디오 코덱 : H.264+AAC(mp4), Theora+Vorbis(ogv), VP8+Vorbis(webm)
- 오디오 코덱 : AAC(aac), MP3(mp3),Vorbis(ogg),WAVE(wav)
- 웹서버에서 MIME 형식에 오디오, 비디오 파일명을 등록해줘야 재생이 된다.
* * *

## 5. source 요소
- 다양한 브라우저 지원을 위해 여러개의 파일을 지정하는 video, audio 요소의 자식 요소
- 이 요소를 사용하는 경우 부모 요소에 src 속성을 지정하면 안됨
- type 속성은 필수는 아니지만 없으면 미디어 리소스를 무조건 내려받아 불필요한 트래픽을 발생하므로 지정을 권함
- 코덱도 지정하면 좋다
* * *

## 6. MIME 형식으로부터 재생 가능 여부를 판단
- media.canPlayType(type) : 매개변수(type)에 MIME 형식을 지정하면 미디어요소가 재생할 수 있는 형식인지 문자열로 반환/ 재생 가능하면 "maybe" 또는 "probably" 반환
* * *

## 7. 선택된 파일 확인
- media.currentSrc : 선택한 미디어 파일의 URL 반환. 없으면 공백 문자열 반환. 읽기 전용
* * *

## 8. 네트워크 이용 상태 파악
- networkState : 미디어 요소의 네트워크 이용상태를 수치로 반환
	- 0 : 선택전 초기 상태 (NETWORK_EMPTY)
	- 1 : 유휴 상태 (NETWORK_IDLE)
	- 2 : 미디어 리소스 로딩중 (NETWORK_LOADING)
	- 3 : 찾을 수 없는 경우 (NETWORK_NO_SOURCE)
	- 브라우저 버전별로 반환되는 숫자값이 다른 경우가 있다. 
- media.NETWORK_EMPTY :  항상 0 반환
- media.NETWORK_IDLE : 항상 1 반환
- media.NETWORK_LOADING : 항상 2 반환
- media.NETWORK_NO_SOURCE : 항상 3 반환
- 미디어 데이터를 내려받으면 최종적으로 NETWORK_IDLE 상태로 끝난다.
* * *

## 9. 재생과 정지
- media.play() : 현재 상태에서 미디어 리소스 재생. 현재 재생 위치에 상관없이 맨 처음부터 재생
- media.pause() : 미디어 리소스의 재생을 멈춘다.
- media.paused : 미디어의 재생이 정지되어 있으면 true, 실행중이면 false 반환 / 읽기 전용
- media.ended : 미디어의 재생이 종료(재생 위치가 맨 뒤)되어 있으면 true, 실행중이면 false 반환 / 읽기 전용
- ios에서 autoplay, preload, load(), play() 함수는 로딩과 동시에 자동재생 되지 않는다.
* * *

## 10. 미디어 리소스 로드
- media.load() : 미디어 요소를 재설정하여 미디어 리소스의 설정을 다시 지정하고 리소스를 로드
- preload 속성 지정되지 않으면 브라우저가 필요한 만큼 리소스를 다시 읽어들임
- 이 함수를 사용하여 리소스를 로드하면 재생 시점이 항상 처음으로 이동
- 브라우저는 미디어 데이터를 서버에서 내려받을 때 필요한 데이터를 우선적으로 다운로드하는데 리소스의 맨처음과 마지막 부분을 내려받는다. (리소스의 길이 정보 때문)
- Range 헤더 - 얻고 싶은 바이트의 범위를 지정
* * *

## 11. 미디어 데이터의 로드 상태 파악
- media.readyState : 현재의 재생 위치에 관한 미디어 데이터의 로드 상태를 나타내는 숫자를 반환 / 읽기 전용
	- 0 : 이용 가능한 미디어 리소스의 정보르르 얻지 못하거나 현재 재생 위치를 재생하기 위한 미디어 데이터를 얻을 수 없는 상태(HAVE_NOTHING)
	- 1 : 미디어 리소스의 길이(시간)와 크기(비디오에 해당)는 얻었으나 아직 현재의 재생 위치를 재생하기 위한 미디어 데이터를 얻지 못한 상태 (HAVE_METADATA)
	- 2 : 현재의 재생 위치만을 표시할 수 있는 미디어 데이터는 얻었지만 아직 현재의 재생 위치에서 재생을 진행하기 위한 데이터는 얻지 못한 상태(HAVE_CURRENT_DATA)
	- 3 : 현재의 재생 위치에서 재생을 진행하기 위한 미디어 데이터는 얻었지만 재생 속도에 맞춰 매끄럽게 재생할 수 있을 만큼의 데이터는 얻지 못한 상태(HAVE_FUTURE_DATA)
	- 4 : 재생 속도에 맞춰 매끄럽게 재생할 수 있을 만큼의 미디어 데이터를 얻은 상태(HAVE_ENOUGH_DATA)
- media.HAVE_NOTHING : 항상 0 반환
- media.HAVE_METADATA : 항상 1 반환
- media.HAVE_CURRENT_DATA : 항상 2 반환
- media.HAVE_FUTURE_DATA : 항상 3 반환
- media.HAVE_ENOUGH_DATA : 항상 4 반환
* * *

## 12. 재생 속도
- media.playbackRate : 재생 속도의 비율을 나타내는 수치 반환 / 기본값 1.0 (보통속도 - 1배속) / 변경 가능 / 음수를 지정하면 되감는다
- media.defaultPlaybackRate : 기본 재생 속도 비율 나타내는 값 반환 / 값을 변경해도 영향 없음./ 재생을 다시 시작하면 이 프로퍼티의 값의 속도로 재생
* * *

## 13. 길이의 재생 위치
- media.duration : 미디어 리소스의 길이(시간)를 초 단위로 반환 / 재생 불가할 경우 NaN 반환 / 스트리밍은 infinity 반환 / 읽기 전용
- media.currentTime : 현재 재생 위치를 초 단위로 반환 / 값 변경 가능
- media.initialTime : 미디어 리소스 로드를 완료했을 때 맨 처음 재생할 위치를 초 단위로 반환 / 보통은 0 반환 / 읽기 전용
- media.startOffsetTime :
	- 선택된 미디어 데이터가 타임라인 정보로, 날짜, 시간을 포함하는 타임 스탬프를 가질 때  미디어의 초기 재생 위치의 일시를 나타내는 Date 객체를 반환
	- 타임 스탬프를 가지지 않을 때는 NaN 반환
	- 읽기 전용
* * *

## 14. 재생 완료와 버퍼링 완료의 범위
- media.played : 이미 재생이 완료된 미디어 리소스의 범위를 나타내는 TimeRanges 객체 반환
- media.buffered : 이미 내려받기가 완료되어 버퍼링된 미디어 리소스의 범위를 나타내는 TimeRanges 객체 반환
- TimeRanges.length : TimeRanges 객체에 담긴 범위의 수를 반환
- TimeRanges.start(index) : TimeRanges 객체에 담긴 범위 중에 매개변수(index)번째 범위의 시작 시간 반환.
- TimeRanges.end(index) : TimeRanges 객체에 담긴 범위 중에 매개변수(index)번째 범위의 종료 시간 반환.
* * *

# 5장 텍스트 편집

- - -
1. 편집 가능한 문서와 요소 [바로가기](https://gitlab.com/choan/HTML5API#1-%ED%8E%B8%EC%A7%91-%EA%B0%80%EB%8A%A5%ED%95%9C-%EB%AC%B8%EC%84%9C%EC%99%80-%EC%9A%94%EC%86%8C)
2. TextSelection API [바로가기](https://gitlab.com/choan/HTML5API#2-text-selection-api)
3. Editing API [바로가기](https://gitlab.com/choan/HTML5API#3-editing-api)
4. 명령 [바로가기](https://gitlab.com/choan/HTML5API#4-%EB%AA%85%EB%A0%B9)
5. 커스텀 WISYWIG 편집기 [바로가기](https://gitlab.com/choan/HTML5API#5-%EC%BB%A4%EC%8A%A4%ED%85%80-wisywig-%ED%8E%B8%EC%A7%91%EA%B8%B0)

* * *


## 1. 편집 가능한 문서와 요소
- 요소를 편집 가능하게 하기 - 모든 요소에 contenteditable 속성을 넣는다. contenteditable = "**true** | **false** | **값없음**"
- 브라우저 내부에 "**inherit**" 상태가 있으며, contenteditable 속성을 지정하지 않으면 기본값으로 지정된다. 부모요소의 편집 여부를 상속한다. 부모요소 편집 가능 상태이면 자식 요소도 편집
-  [예제 소스](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/editable.html)

* * *


## 2. Text Selection API
- 선택한 영역 추출 - Text Selection API는 선택된 텍스트의 영역을 **Selection 객체**로 제공한다. [예제 소스](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/mouseSelect.html)
- window.getSelection(), document.getSelection() - 선택한 텍스트 영역을 나타내는 Selection 객체를 반환. toString()으로 문자열로 변환할 수 있음
- document 객체의 getSelection()은 브라우저 별로 반환하는 값이 다르므로 호환성을 고려하면 **window 객체의 것**을 사용할 것
- 파이어폭스 브라우저와 같이 복수 영역을 선택할 경우를 위해서 Selection 객체의 getRange() 함수를 이용하여 Range객체를 얻어낼 수 있다.
- 선택 영역의 시작 위치와 종료 위치 -
	- **selection.isCollapsed** : 아무것도 선택되지 않으면 true. 무엇인가 선택되었다면 false 반환. 읽기 전용으로 값을 지정할 수 없음
	- **selection.anchorNode** : 선택 영역의 시작 위치를 포함한 **DOM 노드 객체(텍스트노드)**를 반환. 아무것도 선택되어 있지 않으면 null 반환 / 읽기 전용 / 선택영역이 여러개면 마지막 선택 영역이 반환 대상이된다.
	- **selection.anchorOffset** : 선택 영역의 시작 위치를 포함한  **DOM 노드 객체(텍스트노드)**를 기준으로 해당 선택 영역의 시작 위치 오프셋 값 반환. 아무것도 선택되지 않으면 0. /읽기 전용/ 선택영역이 여러개면 마지막 선택 영역이 반환 대상이된다.
	- **selection.focusNode** : 선택 영역의 종료 위치를 포함한  **DOM 노드 객체(텍스트노드)**를 반환. 아무것도 선택되어 있지 않으면 null 반환 / 읽기 전용 / 선택영역이 여러개면 마지막 선택 영역이 반환 대상이된다.
	- **selection.focusOffset** : 선택 영역의 종료 위치를 포함한 **DOM 노드 객체(텍스트노드)**를 기준으로 해당 선택 영역의 종료 위치 오프셋 값 반환. 아무것도 선택되지 않으면 0. /읽기 전용/ 선택영역이 여러개면 마지막 선택 영역이 반환 대상이된다.
	- [예제 소스](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/selectionOffset.html)

- 선택 영역의 지정과 해제
	- **selection.selectAllChildren(parentNode)** - 현재 선택 영역을 매개변수로 지정한 요소 안에 있는 텍스트 전체로 변경
	- **selection.deleteFromDocument()** - 현재의 선택 영역을 해제합니다.
	- [예제 소스](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/selectionAll.html)

- 선택 영역을 해제하고 커서의 위치 이동하기
	- **selection.collapse(parentNode, offset)** - 현재의 선택 영역을 해제. 그리고 DOM노드의 시작 위치와 종료 위치를 첫번째 매개변수(parentNode)와 두번째 매개변수(offset)로 지정. 즉, 빈 선택 영역이 만들어지고 커서를 지정한 위치로 옮긴다.
	- **selection.collapseToStart()** - 현재 선택 영역의 종료 위치를 시작 위치와 같게 하여 선택 영역을 없앤다. 커서의 위치를 선택된 영역의 시작 위치로 옮긴다.
	- **selection.collpaseToEnd()** - 현재 선택 영역의 시작 위치를 종료 위치와 같게 하여 선택 영역을 없앤다. 커서의 위치를 선택된 영역의 종료 위치로 옮긴다.
	- [예제 소스](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/collapseTo.html)

- 여러 개의 선택 영역 다루기
	- **selection.rangeCount** - 선택 영역의 수를 반환. 읽기전용
	- **selection.getRangeAt(index)** - 매개 변수(index)번째에 담긴 선택 영역을 나타내는 range 객체를 반환. index는 0부터 시작
	- **selection.addRange(range)** - 매개변수(range) 객체를 현재 선택 영역의 리스트에 추가
	- **selection.removeRange(range)** - 매개변수(range) 객체에 해당하는 범위를 현재 선택 영역 리스트에서 제거
	- **selection.removeAllRanges()** - 현재 선택 영역 리스트에 있는 모든 선택 영역을 제거

* * *

## 3. Editing API
- WISYWIG 편집기를 구현 - Editing API 를 이용하여 WISYWIG 편집기 구현 가능
- document.execCommand(commandID) / document.execCommand(commandID , showUI) / document.execCommand(commandID , showUI , value) : 
    - 첫번째 매개변수 (commandID)에 지정한 명령을 현재 텍스트 선택 영역에 실행.
    - 두번째 매개변수, 세번째 매개변수는 첫번째 매개변수에 지정된 commandID에 의존. 일반적으로 두번째 매개변수는 명령에 대한 UI를 표시할 지 여부를 true / false로 지정. 
    - 세번째 매개변수는 명령이 필요로 하는 추가 정보를 지정
- document.queryCommandSupported(commandID) : 매개변수 (commandID)에 해당하는 작업을 **브라우저**가 지원하는 경우 true 반환. 반대는 fasle 반환
- document.queryCommandEnabled(commandID) : 매개변수(commandID)에 해당하는 처리가 **이 함수를 호출**할 때 실행 가능하면 true. 반대는 false 반환
- document.queryCommandindeterm(commandID) : (indeterminate : 불확정한, 애매한, 막연한) 매개변수에 해당ㅎ는 작업의 실행 가능 여부를 결정할 수 없으면 true, 반대는 false 반환. 기본적으로 HTML5 명세에 규정되어 있는 명령은 항상 false를 반환
- document.queryCommandState(commandID) :  매개변수에 해당 작업을 실행할때 어떤 상태를 true, false로 반환. 대부분의 명령에 이 함수는 의미가 없는데(대부분 false를 반환하지만) bold, italic, subscript, superscript일때 특정상태에 따라 true, false를 반환
- document.queryCommandValue(commandID) : 매개변수에 해당 작업을 실행할때 어떤 값을 반환한다. 이 값은 매개변수에 따라 다르다. 대부분의 명령에 **문자열 "false"를 반환**하지만, **commandID가 bold, italic, subscript, superscript**일때 queryCommandState() 함수의 값으로 연동한 **문자열 "true" "false"를 반환**
- **파이어폭스는 다른 브라우저와 크게 차이가 있으므로 확인이 필요**
- [예제 소스](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/edit_bold.html)
- 예제에서 iframe을 사용하는 이유
    - 브라우저 호환성 : contentEditable 속성이 "true"인 div를 사용하면 사파리 5.0에서는 execCommand() 함수를 실행할때 브라우저가 강제 종료되는 현상이 있다.
    - 보안적인 측면 : 전체 html 에 영향을 끼칠 수 있는 코드가 실행될 수 있으므로 브라우저 자체적으로 안전할 수 있도록 iframe으로 분리하는 것이 좋다
    - 스타일링 분리 : 편집 영역과 그 외의 영역의 스타일링이 중복 적용되는 것을 막을 수 있다.
- HTML 코드가 완전히 파악 가능한 상황에는 contentEditable 컨텐트 속성을 사용해도 괜찮다.

* * *
## 4. 명령
commandId가 나타내는 명령은 WISYWIG 편집기에서 버튼을 눌렀을 때의 처리에 해당
- **bold**
	- 선택한 텍스트를 굵게 하거나, 원래대로
	- document.execCommand("bold", false, null) - 2,3번째 인자는 사용하지 않음(공통적용)
	- document.queryCommandState("bold") - 선택한 텍스트나 커서가 b요소 안에 있으면 true, 없으면 false / true일때 bold 해제, false를 반환할때 bold 지정
	- document.queryCommandValue("bold") - document.queryCommandState("bold") 반환 값이 true이면 **문자열 "true"**, 그렇지 않으면 **문자열 "false"** 반환
	
- **italic**
	- 선택한 텍스트를 이탤릭으로 하거나 해제
	- document.execCommand("italic", false, null)
	- document.queryCommandState("italic ") - 선택한 텍스트나 커서가 i요소 안에 있으면 true, 없으면 false / true일때 italic 해제, false를 반환할때 italic 지정

- **subscript(아래첨자)**
	- 선택된 텍스트 영역을 아래 첨자로 하거나 해제
	- document.execCommand("subscript", false, null)
	- document.queryCommandState("subscript ") - 선택한 텍스트나 커서가 sub요소 안에 있으면 true, 없으면 false / true일때 아래 첨자 해제, false를 반환할때 아래 첨자 지정
	
- **subscript(아래첨자)**
	- 선택된 텍스트 영역을 아래 첨자로 하거나 해제
	- document.execCommand("subscript", false, null)
	- document.queryCommandState("subscript ") - 선택한 텍스트나 커서가 sub요소 안에 있으면 true, 없으면 false / true일때 아래 첨자 해제, false를 반환할때 아래 첨자 지정
	
- **superscript(위 첨자)**
	- 선택한 텍스트 영역을 위 첨자로 하거나 해제
	- document.execCommand("superscript", false, null)
	- document.queryCommandState("superscript") - 선택한 텍스트나 커서가 sup요소 안에 있으면 true, 없으면 false / true일때 위 첨자 해제, false를 반환할때 위 첨자 지정

- **delete(커서 앞의 문자를 삭제)**
	- 선택한 텍스트 영역을 삭제. 윈도용 키보드에서 backspace(←) 키, 맥용 키보드 delete 키 누른 효과 (선택 영역이 없을 때 커서 앞의 한 문자가 삭제)
	- document.execCommand("delete", false, null)

- **forwardDelete(커서 뒤의 문자를 삭제)** *책 오타
	- 선택한 텍스트 영역을 삭제. 윈도용 키보드의 delete 키 누른 효과 (선택 영역이 없을 때 커서 뒤의 한 문자가 삭제)
	- document.execCommand("forwardDelete", false, null)
	- ie9, 파폭 5.0, 오페라 11에서 아무 동작 안함

- **insertImage(이미지 삽입)**
	- 이미지를 새로 생성하고 텍스트 선택 영역을 해당 이미지로 바꿈. 커서가 있으면 그 위체에 이미지가 삽입
	- 이미지 url을 사용자에게 물어보고 삽입 : document.execCommand("insertImage", true, null) - 이미지를 새로 삽입하려면 2번째 매개변수를 true (일부 브라우저에서 대화상자 표시되지 않음)
	- 이미지 url을 지정하여 이미지 삽입 : document.execCommand("insertImage", false, url) - 2번째 매개변수를 fasle, 3번째 매개변수를 url 문자열로 지정하면 사용자에게 통보없이 링크 생성 (ie9에서는 선택 영역 없으면 이미지 삽입 안됨)

- **insertHTML(HTML 코드 삽입)**
	- 선택한 텍스트 영역을 지정한 HTML 코드로 바꿈(선택 영역이 없고 커서만 있으면 해당 위치에 지정된 HTML 코드를 삽입)
	- document.execCommand("insertHTML", false, HTML코드) - 3번째 매개변수는 필수(문자열로 지정), 2번째 매개변수는 false 지정 빼먹으면 안됨
	- ie9에서 안됨

- **insertText(텍스트 삽입)** *책 오타
	- 커서 위치에서 텍스트를 삽입
	- document.execCommand("insertText", false, 삽입할 text) - 3번째 매개변수는 필수(문자열로 지정 / 지정된 text는 html 코드가 아닌 텍스트 노드로 삽입), 2번째 매개변수는 false 지정 빼먹으면 안됨

- **insertLineBreak(줄바꿈 삽입)**
	- 커서 위치에서 줄바꿈
	- document.execCommand("insertLineBreak", false, null) - 2,3번째 인자 사용 안함

- **insertOrderedList(순서 목록 삽입)**
	- 선택된 텍스트 영역을 ol 요소, li 요소를 사용하여 순서 목록으로 구성
	- document.execCommand("insertOrderedList", false, null) - 2,3번째 인자 사용 안함

- **insertUnorderedList(순서 목록 삽입)**
	- 선택된 텍스트 영역을 ul 요소, li 요소를 사용하여 비순서 목록으로 구성
	- document.execCommand("insertUnorderedList", false, null) - 2,3번째 인자 사용 안함

- **formatBlock(블록 요소의 치환)**
	- 편집 가능한 컨텐츠 중에서 선택한 텍스트의 부모 요소를 지정된 형식 블록 후보 요소로 바꿈
	- document.execCommand("formatBlock", false, 블록요소) 3번째 매개변수는 필수(문자열로 지정), 2번째 매개변수는 false 지정 빼먹으면 안됨
	- 3번째 인자에 들어갈 수 있는 블록 요소 - section. nav, article, aside, h1 ~ h6, hgroup, header, footer, address, p, pre, blockquote, div
	- 이 외의 요소를 3번째 인자로 지정하면 함수 호출은 무시
	- 선택된 텍스트를 기준으로 편집 가능한 영역 안에 포맷 블록 후보 요소인 부모 요소가 없어도 함수 호출 무시
	- 선택한 텍스트를 포함하는 텍스트 노드가 후보 블록 요소가 아니면 상위 부모를 검색하여 블록 요소 치환 대상을 찾는다.
	- 브라우저마다 결과가 조금씩 다름

- **insertParagraph(블록 분할)**
	- 커서의 위치에서 블록으로 분할
	- document.execCommand("insertParagraph", false, null) - 2,3번째 인자 사용 안함
	- ie9 - 선택 영역이 없으면 동작 안함. 선택 영역이 있는 경우에는 선택 영역을 없애고 그 자리에 빈 p 요소를 삽입
	- 오페라 11 - 텍스트 중간에 커서를 두고 insertParagraph를 실행하면 앞 뒤의 텍스트가 p 요소로 나뉜다.
	- 파폭5.0 - 커서의 위치에서 분할하지 않고, 커서를 포함한 텍스트 전체가 p 요소 안에 포함
	- 그 외의 브라우저 - 커서의 뒤쪽 텍스트를 div 요소로 감싸서 단락을 분할

- **createLink(링크 생성)**
	- 선택한 텍스트 영역에 링크를 연결하거나 해제
	- 링크 URL을 사용자에게 물어보고 링크 생성 : document.execCommand("createLink", **true**, null) 
	- 링크 URL을 직접 지정하여 링크 생성 : document.execCommand("createLink", **false**, **url 경로**)

- **unlink(링크 해제)**
	- 선택한 텍스트가 a 요소의 일부 또는 a 요소를 완전히 포함하면 해당 a 요소의 연결을 해제(a 태그를 삭제)
	- document.execCommand("unlink", false, null) - 2,3번째 인자 사용 안함
	- 링크의 일부만 선택해서 unlink 명령을 실행하면  선택한 텍스트 부분만 해제되어 a 태그가 분리됨

- **selectAll(전체 선택)**
	- 포커스가 닿는 편집 가능한 영역의 컨텐츠 모두를 선택 상태로 변환
	- document.execCommand("selectAll", false, null)- 2,3번째 인자 사용 안함

- **unselect(선택 해제)**
	- 포커스가 닿는 편집 가능한 영역의 컨텐츠의 선택 상태를 해제
	- document.execCommand("unselect", false, null)- 2,3번째 인자 사용 안함

- **undo(실행 취소)**
	- 이전 명령의 실행을 취소
	- document.execCommand("undo", false, null) - 2,3번째 인자 사용 안함

- **redo(다시 실행)**
	- 바로 앞의 실행 취소(undo)를 되돌림
	- document.execCommand("redo", false, null) - 2,3번째 인자 사용 안함  


* * *
## 5. 커스텀 WISYWIG 편집기

- 텍스트
	- 크게
	- 작게
	- bold
	- 밑줄
	- 이탤릭
	- 취소선
	- 글자색
	- 왼쪽 정렬
	- 가운데 정렬
	- 우측정렬
	- 꽉찬정렬
- 목록
	- 순서목록
	- 비순서목록
	- 내어쓰기
	- 들여쓰기
- 인용구 설정
- 링크 설정/해제
- 이미지 추가
- undo
- redo
- [commandID 참조 링크](https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand)
- [소스 예제](https://gitlab.com/choan/HTML5API/blob/master/5.%ED%85%8D%EC%8A%A4%ED%8A%B8%ED%8E%B8%EC%A7%91/custom_editor.html)
- [서버 업로드 예제](http://choan616.dothome.co.kr/study/html5/05_Text_editing/custom_editor/custom_editor.html)

