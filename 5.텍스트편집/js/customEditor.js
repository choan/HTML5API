window.addEventListener("load", function () {
  var div = document.querySelector('div.wisywig');
  var iframe = document.querySelector('iframe');
  var win = iframe.contentWindow;
  var doc = win.document;
  doc.designMode = "on";
  var form = document.forms.item(0);
  var textarea = document.querySelector("textarea");
  var menu = document.querySelector("menu");
  var btns = menu.querySelectorAll(".btn");
  for (var i = 0 ; i < btns.length; i++) {
 	 var btn = btns.item(i);
 	 var command_id = btn.getAttribute("data-command");
 	 if (! is_supported(doc, command_id)) { //지원하지 않는 기능은 버튼 비활성화
 	 	btn.disabled = true;
 	 	continue;
 	 }
 	 btn.addEventListener("click", function (evt) {
 	 	exec_command(evt, win, doc);
    textarea.value = doc.body.innerHTML;
 	 	iframe.focus();
 	 }, false)
  }
  
  form.addEventListener("submit", function() { 
 	textarea.value = doc.body.innerHTML;
 }, false)
}, false)
function exec_command(evt, win, doc) {
  var selection = win.getSelection();  
  if (selection.rangeCount == 0) {
  	return;
  }
  var button = evt.currentTarget;  
  var command_id = button.getAttribute("data-command");

 // if ( ! is_enabled(doc, command_id)) {
 // 	return;
 // }
  if ( ! command_id.match(/^(unlink|undo|redo)$/)) {
  	if(selection.isCollapsed) {
  		return;
  	}
  }
  if (command_id == "insertHTML") { 
    var text = selection.toString();
    var colorForm = document.querySelector(".color"); 
    colorForm.addEventListener("change", function () { //change 이벤트 발생시 컬러값 변경
      var color = colorForm.value;
      var code = '<span style="color:' + color + '">' + text + '</span>';
      doc.execCommand("insertHTML", false, code);
    }, false);    
  }else if (command_id == "insertImage"){
    doc.execCommand(command_id, true, null); //ie9 이상만 되고, 나머지는 null 이 그냥 꽂힌다. 
  } else if( command_id == "createLink") {
  	var url = window.prompt("URL을 입력해주세요", "http://");
  	if (url.match(/^http\:\/\/[a-zA-Z0-9\.\-]+/)){
  		doc.execCommand(command_id, false, url);
  	}
  } else {
  	doc.execCommand(command_id, false, null);
  }
}
	
function is_supported (doc, command_id) { //브라우저가 명령을 지원하는지 확인
 var supported = true;
 try {
 	supported = doc.queryCommandSupported(command_id)
 } catch(e) {};
 return supported;
}
 
function is_enabled(doc, command_id) { //현재 이용 가능한 지 확인
 var supported = true;
 try {
 	supported = doc.queryCommandEnabled(command_id)
 } catch(e) {
 	supported = false;
 };
 return supported;
}
