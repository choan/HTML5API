$(window).on("load", function () {
  var div = $(".wisywig");
  var iframe = $("iframe");
  var win = iframe[0].contentWindow;
  var doc = win.document;
  doc.designMode = "on";
  var textarea = $("textarea");
  var menu = $(".menu");
  var btns = $(".menu button");  
 
  btns.each(function () {
  	var command_id = $(this).data("command");
  	if( ! is_supported(doc, command_id)) {
  		$(this).prop("disabled", true);
  	}
  	$(this).on("click", function (evt) {
  		exec_command(evt, win, doc);
 	 		iframe.focus();
 	 		textarea.val($(doc.body).html())
  	})
  });

var form = $("form");
  form.on("submit", function() {
 	textarea.val($(doc.body).html())
 });
});
function exec_command(event, win, doc) {
  var selection = win.getSelection();

  if (selection.rangeCount == 0) {
  	return;
  }
  var button = event.currentTarget;
  var command_id = $(button).data("command");
  if ( ! is_enabled(doc, command_id)) {
  	return;
  }
  if ( ! command_id.match(/^(unlink|undo|redo)$/)) {
  	if(selection.isCollapsed) {
  		return;
  	}
  }
  if( command_id == "createLink") {
  	var url = window.prompt("URL을 입력해주세요", "http://");
  	if (url.match(/^http\:\/\/[a-zA-Z0-9\.\-]+/)){
  		doc.execCommand(command_id, false, url);
  	}
  } else {
  	doc.execCommand(command_id, false, null);
  }
}
	
function is_supported (doc, command_id) {
 var supported = true;
 try {
 	supported = doc.queryCommandSupported(command_id)
 } catch(e) {};
 return supported;
}
 
function is_enabled(doc, command_id) {
 var supported = true;
 try {
 	supported = doc.queryCommandEnabled(command_id)
 } catch(e) {
 	supported = false;
 };
 return supported;
}
